FROM composer:1.10
WORKDIR /app
COPY . /app
RUN composer install --ignore-platform-reqs

FROM node:12
WORKDIR /app
COPY . /app
RUN yarn install && \
    yarn build:prod

FROM php:7.4-cli
WORKDIR /app
COPY --from=0 /app .
COPY --from=1 /app/encore ./encore
RUN ls
RUN mkdir tmp && \
    mkdir zones
CMD ["php", "index.php"]