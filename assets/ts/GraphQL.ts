export class GraphQL {
  private url: string = '';
  public query: string = '';
  public variables: any = {};

  public constructor(url: string) {
    this.url = url;
  }

  public addVariable(key: string, value: any, overwrite: boolean = false) {
    // Make sure we don't overwrite the variable
    if(this.variables.hasOwnProperty(key) && overwrite === false) throw new Error(`Variable "${key}" is already set`);

    // Add the variable
    this.variables[key] = value;

    // Allow chaining
    return this;
  }

  public execute(): Promise<any> {
    return fetch('/graphql', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({query: this.query, variables: this.variables})
    })
      .then((r: any) => r.json());
  }
}