import { GraphQL } from './GraphQL';

class App {
  public constructor() {
    // Add an event listener to the form
    document.querySelector('#domainSubmissionForm')?.addEventListener('submit', this.submitDomain);
  }

  public submitDomain(e: Event) {
    // Prevent refresh
    e.preventDefault();
    
    // Create our formdata
    const form = <HTMLFormElement>document.querySelector('#domainSubmissionForm');
    const fd: FormData = new FormData(form);

    // Create a new GraphQL
    let gql = new GraphQL('/graphql');

    // Create our query
    gql.query = `mutation ($domain: String!, $address: String!, $owner: String) {
      addDomain(domain: $domain, address: $address, owner: $owner) {
        success,
        message
      }
    }`;

    // Add our variables
    gql.addVariable('domain', fd.get('domain'))
       .addVariable('address', fd.get('address'))
       .addVariable('owner', fd.get('owner'));

    // Fetch out data
    gql.execute()
      .then((response: any) => {
        alert(response.data.addDomain.message);
      });
  }

  public getDomains(page: number = 0) {
    // Create a new GraphQL
    let gql = new GraphQL('/graphql');

    // Create our query
    gql.query = `query($page: Int) {
      domains(page: $page) {
        domain,
        address,
        added,
        updated,
        owner
      }
    }`;

    // Fetch our data
    gql.execute()
      .then((response: any) => {
        var tableRef = document.getElementById('domainslist')?.getElementsByTagName('tbody')[0];
        response.data.domains.forEach((domain: any) => {
          let row = tableRef?.insertRow();
          if(!row) return;
          let col = row.insertCell(0);
          col.innerText = domain.domain;
          col = row.insertCell(1);
          col.innerText = domain.added;
          col = row.insertCell(2);
          col.innerText = domain.owner;
          col = row.insertCell(3);
          col.innerText = 'Update Remove';
        });
      });
  }
}

(function() {
  const app: App = new App();
  app.getDomains(0);
})();
