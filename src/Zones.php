<?php
  namespace App;

  use App\Logging\Logger;

  use Nahid\JsonQ\Jsonq;

  class Zones {
    private static $zones = [];

    /**
     * Load all zone files in the ZONES_DIR
     * 
     * @return void
     */
    public static function load(): void {
      // Check if the ZONES_DIR exists
      // If not, create it
      if(!file_exists(ZONES_DIR)) mkdir(ZONES_DIR, 0755, true);

      // Get a list of all zone files
      // Loop over them
      // Then load each of them
      foreach(glob(ZONES_DIR . '/*.json') as $zone) {
        $zoneName = str_replace('.json', '', $zone);
        $zoneName = str_replace(ZONES_DIR . '/', '', $zoneName);
        Logger::write('Loading zonefile for domain "{0}"...', [
          $zoneName
        ]);
        
        try {
          self::$zones[$zoneName] = new Jsonq($zone);
        } catch (\Exception $e) {
          Logger::write('Zonefile "{0}" contains invalid JSON, skipping...', [
            $zoneName
          ]);
        }
      }
    }

    /**
     * Get all zones registered
     * 
     * @return array
     */
    public static function getZones(): array {
      return self::$zones;
    }

    /**
     * Get a specific zone
     * 
     * @param string $zone The root domain of the zone
     * @return Nahid\JsonQ\Jsonq|null
     */
    public static function getZone(string $zone): ?Jsonq {
      // Check if the zone exists
      if(!array_key_exists($zone, self::$zones)) return null;

      // Get and return *a copy* of the zone
      return (self::$zones[$zone])->copy();
    }

    /**
     * Register a zone
     * 
     * @param string $zoneName
     * @param array $data
     * @return void
     */
    public static function addZone(string $zoneName, array $data): void {
      // Add our zone
      self::$zones[$zoneName] = (new Jsonq())->collect($data);

      // Save our zone
      self::saveZone($zoneName);
    }

    /**
     * Save a zone to the designated file
     * 
     * @param string $zoneName
     * @return void
     */
    private static function saveZone(string $zoneName): void {
      $zone = self::$zones[$zoneName];
      if(!$zone) return;

      $json = $zone->toJson();
      if(!$json) return;
      file_put_contents(ZONES_DIR . '/' . $zoneName . '.json', $json);
    }

    /**
     * Get the root from our domain
     * - example.com -> example.com
     * - sub.example.com -> example.com
     * - sub.example.co.uk -> example.co.uk
     * 
     * @param string $domain The domain
     * @return string
     */
    public static function getRoot(string $domain): string {
      $host = strtolower(trim($domain));
      $count = substr_count($host, '.');
      if($count === 2){
        if(strlen(explode('.', $host)[1]) > 3) $host = explode('.', $host, 2)[1];
      } else if($count > 2){
        $host = get_domain(explode('.', $host, 2)[1]);
      }
      return $host;
    }
  }