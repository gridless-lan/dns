<?php
  namespace App\Http;

  use App\Main;
  Use App\Logging\Logger;
  use App\Routing\Router;

  use React\Http\Server as HttpServer;
  use React\Socket\Server as SocketServer;
  use React\Http\Message\Response;
  use Psr\Http\Message\ServerRequestInterface;

  class Webserver {
    private $server;
    private $socket;

    public function __construct(string $host = '0.0.0.0', int $port = 8080) {
      // Create a socket for our webserver
      $this->socket = new SocketServer($host . ':' . $port, Main::getLoop());

      // Create our basic webserver
      $this->server = new HttpServer(Main::getLoop(), function (ServerRequestInterface $request) {
        // Log some debug stuff
        Logger::write('Incoming {0} from "{1}" on "{2}"', [
          $request->getMethod(),
          $request->getServerParams()['REMOTE_ADDR'],
          $request->getUri()->getPath()
        ]);

        // Get the requested route
        $route = Router::match($request);

        // Initialize the required controller
        $controller = new $route['controller']($request, $route);
        
        // Run the required action
        $body = call_user_func_array([$controller, $route['action']], []);

        // Send our response
        return $controller->getResponse();
      });

      // Add our error handler
      $this->server->on('error', function(\Throwable $e) {
        Logger::write('Error: {0}', [
          $e
        ]);
      });

      // Set our listen port
      $this->server->listen($this->socket);
    }
  }