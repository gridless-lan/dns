<?php
  define('DEBUG', true);
  define('APP_DIR', __DIR__);
  define('TMP', dirname(__DIR__) . '/tmp');
  define('ZONES_DIR', dirname(__DIR__) . '/zones');
  define('CA_CERTIFICATE', dirname(__DIR__) . '/cacert.pem');
  define('ASSET_DIR', dirname(__DIR__) . '/encore');