<?php
  namespace App\DNS;

  use React\Dns\Model\Message;
  use React\Dns\Protocol\Parser as ReactParser;

  final class Parser {
    public function parseMessage($data, $mustDecode = true) {
      // Decode the base64url
      if($mustDecode) $data = base64_decode(strtr($data, '-_', '+/') . str_repeat('=', 3 - ( 3 + strlen( $data )) % 4 ));

      // Parse the message
      $parser = new ReactParser();
      return $parser->parseMessage($data);
    }
  }