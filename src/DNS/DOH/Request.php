<?php
  namespace App\DNS\DOH;

  class Request {
    public function b64decode($input) {
      // Pad the input if needed
      $padded = str_pad($input, (-strlen($input) % 4), '=');

      //dump($this->decodeDomain(base64_decode($padded)));
      
      // Base64 decode the padded input
      return base64_decode($padded);
    }
  }