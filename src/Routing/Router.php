<?php
  namespace App\Routing;

  use Psr\Http\Message\ServerRequestInterface;
  use Symfony\Component\Routing\Matcher\UrlMatcher;
  use Symfony\Component\Routing\RouteCollection;
  use Symfony\Component\Routing\RequestContext;
  use Symfony\Component\Routing\Route;

  class Router {
    private static $routes;

    /**
     * Add a route to the RouterCollection
     * Instantiates the collection if not done yet
     * 
     * @param string $name
     * @param string $expression
     * @param array $target
     * @param array $methods
     * @return void
     */
    public static function add(string $name, string $expression, array $target, array $methods = []): void {
      // Initialize a new RouterCollection if not done yet
      if(!self::$routes) self::$routes = new RouteCollection();

      // Create our new route
      $route = new Route($expression, $target);

      // If a methods list was specified
      // Set these limitations
      if($methods > 0) $route->setMethods($methods);

      // Add the route to the RouterCollection
      self::$routes->add($name, $route);
    }

    /**
     * Adds a new route to the RouterCollection
     * 
     * @param string $name
     * @param string $expression
     * @param array $target
     * @param array $methods
     * @return void
     */
    public static function connect(string $name, string $expression, array $target, $methods = []): void {
      self::add($name, $expression, $target, $methods);
    }

    /**
     * Adds a new GET-only route to the RouterCollection
     * 
     * @param string $name
     * @param string $expression
     * @param array $target
     * @return void
     */
    public static function get(string $name, string $expression, array $target): void {
      self::add($name, $expression, $target, ['GET']);
    }

    /**
     * Adds a new POST-only route to the RouterCollection
     * 
     * @param string $name
     * @param string $expression
     * @param array $target
     * @return void
     */
    public static function post(string $name, string $expression, array $target): void {
      self::add($name, $expression, $target, ['POST']);
    }

    /**
     * Match the requested url to a route
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return array The route array
     */
    public static function match(ServerRequestInterface $request): array {
      // Build our request context
      $context = new RequestContext();
      $context->setMethod($request->getMethod());

      // Create a new matcher
      $matcher = new UrlMatcher(self::$routes, $context);

      // Match the route
      return $matcher->match($request->getUri()->getPath());
    }
  }