<?php
  use App\Routing\Router;
  use App\Controller\AssetController;
  use App\Controller\DnsController;
  use App\Controller\GraphqlController;
  use App\Controller\HomeController;

  Router::get('home', '/', ['controller' => HomeController::class, 'action' => 'index']);
  Router::post('graphql', '/graphql', ['controller' => GraphqlController::class, 'action' => 'index']);
  Router::get('asset', '/{dir}/{name}.{ext}', ['controller' => AssetController::class, 'action' => 'index']);
  Router::connect('dns', '/dns-query', ['controller' => DnsController::class, 'action' => 'index'], ['GET', 'POST']);