<?php
  namespace App\Controller;

  use App\Controller\AppController;

  class HomeController extends AppController { 
    public function index() {

      // Add our domains
      // TODO: Use the API for this
      $this->set('domains', [
        [
          'root' => 'gridless-lan.nl',
          'added' => 'N/A',
        ],
      ]);

      // Render the view
      $this->render();
    }
  }