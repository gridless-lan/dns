<?php
  namespace App\Controller;

  use Psr\Http\Message\ServerRequestInterface;
  use React\Http\Message\Response;
  use Twig\Loader\FilesystemLoader;
  use Twig\Environment as TwigEnvironment;
  use React\Http\Io\HttpBodyStream;

  class AppController {
    private $status = 200;
    protected $request;
    private $response;
    private $route;
    private $twig;
    private $layout = 'default';
    private $viewVars = [];
    private $rendered = null;

    public function __construct(ServerRequestInterface $request, array $route) {
      // Store our request
      $this->request = $request;

      // Store our route
      $this->route = $route;

      // Initialize Twig
      $twigLoader = new FilesystemLoader(APP_DIR . '/View/Template');
      $this->twig = new TwigEnvironment($twigLoader, [
        'cache' => TMP . '/cache/twig/',
        'debug' => DEBUG,
      ]);
    }

    /**
     * Set a layout to use
     * 
     * @param string $layout The name of the layout you want to use
     * @return void
     */
    protected function setLayout(string $layout = 'default'): void {
      $this->layout = $layout;
    }

    /**
     * Set a view variable
     * 
     * @param string $name The name of the variable
     * @param any $value The value of the variable
     * @return void
     */
    protected function set(string $name, $value): void {
      $this->viewVars[$name] = $value;
    }

    /**
     * Render the view
     * 
     * @return void
     */
    protected function render(): void {
      // Render our page content
      $replaceCount = 1;
      $viewFolder = str_replace('Controller', '', $this->route['controller'], $replaceCount);
      $replaceCount = 1;
      $viewFolder = str_replace('App\\', '', $viewFolder, $replaceCount);
      $content = $this->twig->render($viewFolder . '/'. $this->route['action'] . '.html', $this->viewVars);
      $this->set('content', $content);

      // Render our final body
      $body = $this->twig->render('Layout/'.$this->layout.'.html', $this->viewVars);

      // Set out response
      $this->setResponse(new Response(
        $this->status,
        array(
          'Content-Type' => 'text/html'
        ),
        $body
      ));
    }

    /**
     * Output the page as JSON
     * 
     * @return void
     */
    protected function json(array $data): void {
      // Set out response
      $this->setResponse(new Response(
        $this->status,
        array(
          'Content-Type' => 'application/json'
        ),
        json_encode($data)
      ));
    }

    /**
     * Output raw data with a specified MIME
     * 
     * @param string $mime The desired MIME type
     * @param any $data
     * @return void
     */
    protected function raw(string $mime = 'text/plain', $data = ''): void {
      $this->setResponse(new Response(
        $this->status,
        array(
          'Content-Type' => $mime
        ),
        $data
      ));
    }

    /**
     * Get the response object
     * 
     * @return React\Http\Message\Response
     */
    public function getResponse(): Response {
      return $this->response;
    }

    /**
     * Set the response object
     * 
     * @param React\Http\Message\Response $response The response object
     * @return void
     */
    protected function setResponse(Response $response): void {
      $this->response = $response;
    }

    /**
     * Check if we are dealing with a POST request
     * 
     * @return boolean True if POST request
     */
    protected function isPost(): boolean {
      return $this->request->getMethod() === 'POST';
    }

    /**
     * Set the HTTP Status Code
     *
     * @param int $status The status code
     * @return void
     */
    protected function setStatus(int $code): void {
      $this->status = $code;
    }

    /**
     * Get the current route
     * 
     * @return array
     */
    protected function getRoute(): array {
      return $this->route;
    }
  }