<?php
  namespace App\Controller;

  use App\Controller\AppController;

  class AssetController extends AppController { 
    protected $mimeTypes = [
      'js' => 'application/javascript',
    ];

    public function index() {
      // Check if the file exists
      if(!file_exists(ASSET_DIR . '/' . $this->getRoute()['name'] . '.' . $this->getRoute()['ext'])) {
        $this->setStatus(404);
        return $this->raw('text/plain', '404 File not found');
      }
       

      // Get the MIME of the file
      if(array_key_exists($this->getRoute()['ext'], $this->mimeTypes)) 
        $mime = $this->mimeTypes[$this->getRoute()['ext']];


      // Send the asset
      $this->raw($mime, file_get_contents(ASSET_DIR . '/' . $this->getRoute()['name'] . '.' . $this->getRoute()['ext']));
    }
  }