<?php
  namespace App\Controller;

  use App\Controller\AppController;
  use App\GraphQL\Types;
  
  use Psr\Http\Message\ServerRequestInterface;
  use GraphQL\GraphQL;
  use GraphQL\Type\Schema;
  use GraphQL\Error\FormattedError;
  use GraphQL\Error\DebugFlag;
  use GraphQL\Language\Parser;
  use GraphQL\Utils\SchemaPrinter;

  class GraphqlController extends AppController {
    private $debug = false;
    private $schema;

    public function __construct(ServerRequestInterface $request, array $route) {
      parent::__construct($request, $route);

      // Check if we are in debug mode
      if(DEBUG) $this->debug = DebugFlag::INCLUDE_DEBUG_MESSAGE | DebugFlag::INCLUDE_TRACE;

      // Create a GraphQL schema
      $this->schema = new Schema([
        'query' => Types::query(),
        'mutation' => Types::mutation(),
      ]);

    }

    public function index() {
      // Check whether we need to get the data from the POST body or GET params
      switch(strtolower($this->request->getMethod())) {
        case 'post':
          $raw = json_decode($this->request->getBody()->getContents(), true);
          break;
        case 'get':
        default:
          // TODO: Implement getting from query
          $raw = '';
          break;
      }

      try {
        // Parse the data
        $parsed = $this->parse($raw['query'], $raw['variables']);

        // Execute our query
        $result = GraphQL::executeQuery(
          $this->schema,
          $parsed['query'],
          null,
          null,
          $parsed['variables']
        );

        // Return the results
        $this->setStatus(200);
        $this->json($result->toArray($this->debug));
      } catch (\Exception $e) {
        $this->setStatus(400);
        $this->json([
          'errors' => [
            FormattedError::createFromException($e, $this->debug)
          ]
        ]);         
      }
    }

    /**
     * Parse the query and variables
     * 
     * @param string $query The Query string
     * @param string $variables JSON of all variables
     * @return array 
     */
    private function parse(string $query = '', $variables = ''): array {
      $parsed = Parser::parse($query);

      return [
        'query' => $parsed,
        'variables' => (is_array($variables) ? $variables : json_decode($variables, 1))
      ];
    }
  }