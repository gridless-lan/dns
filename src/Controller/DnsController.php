<?php
  namespace App\Controller;

  use App\Controller\AppController;
  use App\Zones;
  use App\DNS\DOH\Request;
  use App\DNS\Message;
  use App\DNS\Binary;
  use App\DNS\Query;
  use App\DNS\Record;
  use App\DNS\Parser;

  class DnsController extends AppController { 
    public function index() {
      $parser = new Parser();

      // Parse the incoming message based on method
      switch($this->request->getMethod()) {
        case 'GET':
          // Check if an query param was added
          if(empty($this->request->getQueryParams()['dns'])) return;
          $parsed = $parser->parseMessage($this->request->getQueryParams()['dns'], true);
          break;
        case 'POST':
          if(empty($this->request->getBody())) return;
          $parsed = $parser->parseMessage((string)$this->request->getBody(), false);
          break;
        default:
          return;
      }

      // Build our response message
      $records = $this->getAnswers($parsed->questions[0]->name);
      $query = new Query($parsed->questions[0]->name, Message::TYPE_A, Message::CLASS_IN);
      $request = Message::createResponseWithAnswersForQuery($query, $records);

      // Update the id if need be
      if($parsed->id != 0) $request->id = $parsed->id;

      // Encode the data
      $this->binary = new Binary();
      $queryData = $this->binary->toBinary($request);

      // Send the data to the client
      $this->raw('application/dns-udpwireformat', $queryData);
    }

    public function index2() {
      // Check if a name param was set
      if(empty($this->request->getQueryParams()['name'])) return;

      // Get the answers    
      $answers = $this->getAnswers($this->request->getQueryParams()['name']);

      $data = [
        'Status' => 0,
        'TC' => false,
        'AD' => false,
        'CD' => false,
        'Question' => [
          [
            'name' => $this->request->getQueryParams()['name'] . '.',
            'type' => $this->request->getQueryParams()['type']
          ]
          ],
        'Answer' => [$record->toArray()],
        'Comment' => 'Response from dns.gridless-lan.nl'
      ];

      $this->json($data);
    }

    private function getAnswers(string $name) {
      // Get the zone
      $zoneName = Zones::getRoot($name);
      $zone = Zones::getZone($zoneName);

      // Get the record name
      $recordName = str_replace('.' . $zoneName, '', $name);
      $record = $zone->from('records')->where('name', '=', $recordName)->first()->toArray();

      switch($record['type']) {
        case 'A':
          $type = Message::TYPE_A;
          break;
        default:
          $type = Message::TYPE_A;
          break;
      }

      $result = new Record($record['name'], $type, Message::CLASS_IN, $record['ttl'], $record['content']);
      return [$result];
    }

    private function getQuery() {

    }
  }