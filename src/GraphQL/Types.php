<?php
  namespace App\GraphQL;

  use App\GraphQL\Type\Definition\{
    QueryType,
    MutationType,
    DomainType
  };

  use GraphQL\Type\Definition\{
    Type,
    ObjectType,
    ListOfType,
    IntType,
    BooleanType,
    IDType,
    StringType,
    NonNull
  };
  use GraphQL\Type\Schema;
  use GraphQL\GraphQL;

  class Types {
    private static $query;
    private static $mutation;
    private static $domain;

    /**
     * @return QueryType
     */
    public static function query(): QueryType {
      return self::$query ?: (self::$query = new QueryType());
    }

    /**
     * @return MutationType
     */
    public static function mutation(): MutationType {
      return self::$mutation ?: (self::$mutation = new MutationType());
    }

    /**
     * @return DomainType
     */
    public static function domain(): DomainType {
      return self::$domain ?: (self::$domain = new DomainType());
    }

    /**
     * @param Type $type Type which should be nonNull
     * @return NonNull
     */
    public static function nonNull($type): NonNull {
      return Type::nonNull($type);
    }

    /**
     * @return \GraphQL\Type\Definition\IntType
     */
    public static function int(): IntType {
      return Type::int();
    }

    /**
     * @return \GraphQL\Type\Definition\IDType
     */
    public static function id(): IDType {
      return Type::id();
    }

    /**
     * @return \GraphQL\Type\Definition\StringType
     */
    public static function string(): StringType {
      return Type::string();
    }

    /**
     * @param Type $type
     * @return ListOfType
     */
    public static function listOf($type): ListOfType {
      return new ListOfType($type);
    }

    /**
     * @return \GraphQL\Type\Definition\BooleanType
     */
    public static function boolean(): BooleanType {
      return Type::boolean();
    }
  }
