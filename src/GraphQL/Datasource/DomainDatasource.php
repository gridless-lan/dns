<?php
  namespace App\GraphQL\Datasource;

  use App\Zones;
  use App\Http\CertificateValidator;

  class DomainDatasource {
    /**
     * Get a list of the domains
     * 
     * @return array Array containing the domains
     */
    public function getDomains(int $start, int $end): array {
      return array_slice(Zones::getZones(), $start, $end);
    }

    /**
     * Validate a domain.
     * If validation succeeds, add it to the list of domains
     * 
     * @param string $domain The root domain to be added
     * @param string $address The address the root domain should resolve to
     * @param string $owner The owner of the domain
     * @return array
     */
    public function addDomain(string $domain, string $address, ?string $owner): array {
      // Make sure we get the root domain
      $domain = Zones::getRoot($domain);

      // Download the zone file from the server
      $ch = curl_init("https://".$domain.'/.well-known/gridless-lan/'.$domain.'.json');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_CAINFO, CA_CERTIFICATE);
      curl_setopt($ch, CURLOPT_CAPATH, CA_CERTIFICATE);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt($ch, CURLOPT_RESOLVE, [
        $domain.":443:".$address,
      ]);
      $res = curl_exec($ch);

      // Check if we have a response
      // If not, check what went wrong
      if(!$res) {
        switch(curl_errno($ch)) {
          case 60:
            return ['success' => false, 'message' => 'The certificate was not valid'];
        }
      }

      // Make sure we obtained a zonefile
      if(curl_getinfo($ch, CURLINFO_HTTP_CODE) === 0) return ['success' => false, 'message' => 'Could not connect to server'];
      if(curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) return ['success' => false, 'message' => 'Could not obtain zonefile ('.curl_getinfo($ch, CURLINFO_HTTP_CODE).')'];

      // Parse the zone file
      $zone = json_decode($res, true);
      if(!$zone) return ['success' => false, 'message' => 'Zonefile is not valid JSON'];

      // Get our timestamp
      $ts = time();

      // Start building our final zone
      $final = [
        'domain' => $domain,
        'address' => $address,
        'added' => $ts,
        'updated' => $ts,
        'owner' => $owner,
        'records' => [
          [
            'type' => 'A',
            'name' => $domain,
            'content' => $address,
            'ttl' => 3600
          ]
        ],
      ];

      // Validate each record
      // Making sure it has all the values needed
      // Remove everything else
      foreach($zone as $input) {
        $record = [];

        // Make sure a type is set
        if(empty($input['type'])) {
          continue;
        } else {
          $record['type'] = $input['type'];
        }
        
        // Make sure a name is set
        if(empty($input['name'])) {
          continue;
        } else {
          $record['name'] = $input['name'];
        }

        // Make sure a content is set
        if(empty($input['content'])) {
          continue;
        } else {
          $record['content'] = $input['content'];
        }

        // Make sure a TTL is set
        if(empty($input['ttl'])) {
          continue;
        } else {
          $record['ttl'] = $input['ttl'];
        }

        $final['records'][] = $record;
      }

      // Add our zone
      Zones::addZone($domain, $final);

      return [
        'success' => true,
        'message' => 'Domain has been added!',
      ];
    }
  }