<?php
  namespace App\GraphQL\Type\Definition;

  use App\GraphQL\Types;
  use App\GraphQL\Datasource\DomainDatasource;

  use GraphQL\Type\Definition\ObjectType;
  use GraphQL\Type\Definition\ResolveInfo;

  class DomainType extends ObjectType {
    private $domainDatasource;

    public function __construct() {
      // Instantiate our datasource
      $this->domainDatasource = new DomainDatasource();

      // Setup our type
      $config = [
        'name' => 'Domain',
        'description' => 'Domains registered on this server',
        'fields' => [
          'id' => Types::id(),
          'domain' => [
            'type' => Types::string(),
            'description' => 'Root domain'
          ],
          'address' => [
            'type' => Types::string(),
            'description' => 'The IP this root domain should resolve to'
          ],
          'added' => [
            'type' => Types::int(),
            'description' => 'Unix timestamp for when this domain was added'
          ],
          'updated' => [
            'type' => Types::int(),
            'description' => 'Unix timestamp for when this domain was last updated'
          ],
          'owner' => [
            'type' => Types::string(),
            'description' => 'Name of whom owns this domain'
          ],
        ]
      ];

      parent::__construct($config);
    }

    public function resolveDomains($rootValue, $args, $context, ResolveInfo $info) {
      return $this->domainDatasource->getDomains(0,5);
    }

    public function resolveDomain($rootValue, $args, $context, ResolveInfo $info) {
      return [];
    }

    public function resolveAddDomain($rootValue, $args, $context, ResolveInfo $info) {
      return $this->domainDatasource->addDomain($args['domain'], $args['address'], $args['owner']);
    }
  }
