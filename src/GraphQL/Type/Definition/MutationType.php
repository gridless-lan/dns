<?php
  namespace App\GraphQL\Type\Definition;

  use GraphQL\Type\Definition\ObjectType;
  use GraphQL\Type\Definition\ResolveInfo;
  use GraphQL\Type\Definition\Type;
  use GraphQL\Type\Definition\ListOfType;

  use App\GraphQL\Types;
  use App\GraphQL\Type\Definition\{
    DomainType
  };

  class MutationType extends ObjectType {
    public function __construct() {
      $config = [
        'name' => 'Mutation',
        'fields' => [
          'addDomain' => [
            'args' => [
              'domain' => Types::nonNull(Types::string()),
              'address' => Types::nonNull(Types::string()),
              'owner' => Types::string(),
            ],
            'type' => new ObjectType([
              'name' => 'AddDomainOutput',
              'fields' => [
                'success' => Types::boolean(),
                'message' => Types::string(),
              ]
            ]),
            'resolve' => function($rootValue, $args, $context, ResolveInfo $info) {
              // TODO: Make this more automatic
              // Get an instance of our type
              $instance = Types::domain();

              // Check if the method exists
              // Execute it if so
              // Else, return null
              if (method_exists($instance, 'resolveAddDomain')) return $instance->resolveAddDomain($rootValue, $args, $context, $info);
              return [
                'success' => false,
                'message' => 'Something went wrong while trying to add the domain...',
              ];
            }
          ]
        ]
      ];

      parent::__construct($config);
    }
  }
