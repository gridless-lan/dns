<?php
  namespace App\GraphQL\Type\Definition;

  use GraphQL\Type\Definition\ObjectType;
  use GraphQL\Type\Definition\ResolveInfo;
  use GraphQL\Type\Definition\Type;
  use GraphQL\Type\Definition\ListOfType;

  use App\GraphQL\Types;
  use App\GraphQL\Type\Definition\{
    DomainType
  };

  class QueryType extends ObjectType {
    public function __construct() {
      $config = [
        'name' => 'Query',
        'fields' => [
          'domains' => [
            'type' => Types::listOf(Types::domain()),
            'description' => 'Returns subset of domains available',
            'args' => [
              'page' => Types::int(),
            ],
          ],
        ],
        'resolveField' => function($rootValue, $args, $context, ResolveInfo $info) {
          // Check if we are dealing with a ListOfType
          // If so, strip a trailing 's' from the field name
          // Else, just pass the fields name
          if($info->returnType instanceof ListOfType) {
            $name = rtrim($info->fieldName, 's');
          } else {
            $name = $info->fieldName;
          }

          // Get an instance of our type
          $instance = Types::{$name}();

          // Create our method name
          $method = 'resolve' . ucfirst($info->fieldName);

          // Check if the method exists
          // Execute it if so
          // Else, return null
          if (method_exists($instance, $method)) return $instance->{$method}($rootValue, $args, $context, $info);
          return null;
        }
      ];

      parent::__construct($config);
    }
  }
