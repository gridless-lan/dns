<?php
  namespace App;

  use App\Http\Webserver;
  use App\Logging\Logger;
  use App\Zones;

  use React\EventLoop\Factory;
  use React\EventLoop\StreamSelectLoop;

  class Main {
    private static $config = [
      'webserver' => [
        'host' => '0.0.0.0',
        'port' => 8080,
      ],
    ];
    private static $loop;
    private static $webserver;

    /**
     * Initialize the app
     * - Create an event loop
     * - Load the Database
     * - Initialize a webserver
     * 
     * @return void
     */
    public static function initialize(): void {
      // Create our main loop
      self::$loop = Factory::create();

      // Load our zones
      Zones::load();

      // Initialize our webserver
      Logger::write('Webserver started on port "{0}"...', [
        self::$config['webserver']['port'],
      ]);
      self::$webserver = new Webserver(self::$config['webserver']['host'], self::$config['webserver']['port']);
    }

    /**
     * Start our main loop
     * 
     * @return void
     */
    public static function start(): void {
      self::$loop->run();
    }

    /**
     * Return our main loop
     * 
     * @return React\EventLoop\StreamSelectLoop
     */
    public static function getLoop(): StreamSelectLoop {
      return self::$loop;
    }
  }