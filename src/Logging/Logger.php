<?php
  namespace App\Logging;

  class Logger {
    /**
     * Write out something to the console
     * @param string $message The message that should be dislayed
     * @param array $args Values for placeholders
     */
    public static function write(string $message, array $args = []) {
      // Build our timestamp
      $output = '[';
      $output .= date('Y-m-d h:i:s');
      $output .= '] ';

      // Build the message itself
      foreach($args as $index => $value) {
        $message = str_replace("{".$index."}", $value, $message);
      }

      // Write to the console
      print_r($output . $message . PHP_EOL);
    }
  }