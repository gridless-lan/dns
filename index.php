<?php
  // Include the autoloader
  require_once __DIR__.'/vendor/autoload.php';

  // Load our constants
  require_once __DIR__.'/src/Constants.php';

  use App\Main;

  // Load the routes
  require_once __DIR__.'/src/Routes.php';

  // Initialize the main app
  // Then start it
  Main::initialize();
  Main::start();